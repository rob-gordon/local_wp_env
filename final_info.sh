# Final Information
printf '\e[4;31m\n=============================================\n\e[m'
printf "\n\e[1;34mThe development environment is visible at \033[0;32m$IP\033[0m\n"
printf "\e[1;34mAdd this virtual hosts by running\n"
printf "\033[0;33msudo -- sh -c \"echo '$IP    $URL' >> /etc/hosts\" && echo \"http://$URL\"\033[0m\n"
printf "\e[1;34mCleanup excess files generated by this script with\n\033[0;33msudo ./local_wp_env/local_wp_env.sh --cleanup\033[0m"
printf '\e[4;31m\n\n=============================================\n\n\e[m'