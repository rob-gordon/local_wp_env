__local_wp_env__
============

-----------

### A command line tool to install a vagrant box with a LAMP stack ready for local wordpress development.

__Directions:__ Clone directly into your wordpress install / repo and run the script from the root with `./local_wp_env/local_wp_env.sh`

__Note:__ Running this will overwrite any Vagrantfile (or provision.yml) that's sitting in the root and is potentially part of the repo.




__Optionally :__ You can add an alias to your .bashrc or .zshrc with:

    alias local_wp_env="git clone git@bitbucket.org:rob-gordon/local_wp_env.git && ./local_wp_env/local_wp_env.sh"

### Useful Links

[Pantheon wp-config.php](https://raw.githubusercontent.com/pantheon-systems/wordpress/master/wp-config.php)

----------

## To Do

1. ~~Move creation of wp-local-config.php from __Vagrantfile__ to __local_wp_env.sh__~~
2. ~~Add option to cleanup (check if it's being run with sudo)~~
3. Support for HTTPS://
4. Support for xdebug
5. There might be a recurring error with Content-Type Encoding (see this [question](http://stackoverflow.com/questions/4086717/why-do-symbols-like-apostrophes-and-hyphens-get-replaced-with-black-diamonds-on))
    - Its possible that you may need to edit the box and/or watch out for the meta tag on the __distro__
6. Have guest VM auto install wp cli
7. Re-integrate Ian's specific memory control and other necessities
8. Is it idempotent
9. Consider writing somethign to sync mysql databses into the project folder and gitignore them, just because aborted VM's will happen
10. Consider writing as a command for WP CLI instead
11. At least write an entry point to `vagrant provision` if it's needed