#!/bin/bash
#
#
# This script sets up a local wp development environment using Vagrant.
#
# It's meant to be run in the root of the wordpress install / repo.
#
# If anything goes wrong during installion run 'VAGRANT_LOG=info vagrant provision' to
# attempt setting up the environment again and debug.


# Requirements
# -----------
# Virtual Box
# Vagrant
# Ansible


REPO="https://bitbucket.org/rob-gordon/local_wp_env/get/HEAD.zip"
SITENAME="wordpress"
SUFFIX="local"
URL=$SITENAME.$SUFFIX
GENERATE_CONFIG=false





#####################
# Cleanup
# removes folders that were only needed during provisioning
#####################

if [ $# -eq 1 ] && [ "$1" = "--cleanup" ]
	then
	if [ "$EUID" -ne 0 ]
		then echo "Please run cleanup as root"
		exit
	fi
	rm -r "./local_wp_env"
	rm -r "./html"
	rm "./000-default.conf" "./provision.yml"
	exit
fi

#####################
# Wipe
# completely removes all traces that this script ever did anything
#####################

if [ $# -eq 1 ] && [ "$1" = "--wipe" ]
	then
	if [ "$EUID" -ne 0 ]
		then echo "Please run wipe as root"
		exit
	fi
	rm -r "./local_wp_env" "./html" "./.vagrant"
	rm "./000-default.conf" "./provision.yml" "./Vagrantfile"
	exit
fi





#####################
# Setup
#####################

# Check if in the root of a wp install
if ! [ -d "./wp-content" ]; then
	echo "Please run this script from the root of a wordpress install."
	exit
fi


# If this was run as a command tool and not cloned down
# then we need to download the repo for the resources
if ! [ -d "./local_wp_env" ]; then
	wget -O "./local_wp_env.zip" $REPO
	TMPFOLDER=$(unzip "./local_wp_env.zip" | grep -m1 'creating:' | cut -d' ' -f5-)
	rm "./local_wp_env.zip"
	mv $TMPFOLDER local_wp_env
fi


# move necessary resources to root of install
mv local_wp_env/resources/* ./





#####################
# Begin Interaction
#####################

# Site Information
# ----------------
printf '\e[4;31m\nInformation\n\e[m'

printf '\e[1;34m\n%s\n===========\n\e[m' "Site Name (only letters and dashes)"
read SITENAME

printf '\e[1;34m\n%s\n===========\n\e[m' "URL/Hostname Suffix"
select SUFFIX in 'local' 'dev'
do
	URL=$SITENAME.$SUFFIX
	printf '\e[1;34mThe url/hostname will be \033[0;32m%s\n\e[m' $URL
    break
done


# Environment Configuration
# -------------------------
printf '\e[4;31m\nEnvironment Configuration\n\e[m'


printf '\e[1;34m\n%s\n===========\n\e[m' "PHP Version"
select VERSION in 7.0 5.6
do
    printf '\e[1;34mPHP \033[0;32m%s\n\e[m' $VERSION
    break
done


# Post Install
# ------------
printf '\e[4;31m\nPost Install\n\e[m'

# Edit wp-config-sample.php

read -p $'\e[1;34m\nGenerate wp-config.php? (y/n)\n\e[0m' -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
	# SSH Into the Host and Do This With WP CLI
	GENERATE_CONFIG=true
	if [ -f "./wp-config-sample.php" ]; then
		rm "./wp-config-sample.php"
	fi
fi


# Generate wp-config-local.php (Pantheon)
read -p $'\e[1;34m\nGenerate wp-config-local.php (Pantheon)? (y/n)\n\e[0m' -n 1 -r
echo    
if [[ $REPLY =~ ^[Yy]$ ]]
then
    echo "<?php\n\tdefine('DB_NAME', 'wordpress');\n\tdefine('DB_USER', 'wordpress');\n\tdefine('DB_PASSWORD', 'wordpress');\n\tdefine('DB_HOST', 'localhost');\n" > "wp-config-local.php"
fi




#####################
# Set Up Vagrant
#####################
printf '\e[4;31m\n\nSetting Up Vagrant\n\e[m'
SITENAME=$SITENAME VERSION=$VERSION GENERATE=$GENERATE vagrant up

# Install WP-CLI
printf '\e[4;31m\n\nInstalling WP-CLI\n\e[m'
vagrant ssh -- -t 'cd /tmp && curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar && chmod +x wp-cli.phar && sudo mv wp-cli.phar /usr/local/bin/wp && cd /var/www'

if $GENERATE_CONFIG; then
	printf '\e[4;31m\nGenerating wp-config.php\n\e[m'
	vagrant ssh -- -t 'cd /var/www && wp core config --dbname=wordpress --dbuser=wordpress --dbpass=wordpress'
fi


#####################
# Final Information
#####################

# Get IP Address
IFCONFIG=$(vagrant ssh -- -t 'ifconfig enp0s8')
IP=$(echo "$IFCONFIG" | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}')

printf '\e[4;31m\n=============================================\n\e[m'
printf "\n\e[1;34mThe development environment is visible at \033[0;32m$IP\033[0m\n"
printf "\e[1;34mAdd this virtual hosts by running\n"
printf "\033[0;33msudo -- sh -c \"echo '$IP    $URL' >> /etc/hosts\" && echo \"http://$URL\"\033[0m\n"
printf "\e[1;34mCleanup excess files generated by this script with\n\033[0;33msudo ./local_wp_env/local_wp_env.sh --cleanup\033[0m"
printf '\e[4;31m\n\n=============================================\n\n\e[m'